开源JT1078流媒体服务器

#### 软件架构
 项目采用java的 springboot+javacv+nginx-flv


#### 安装教程
1.打开IDEA，导入项目，等待依赖下载
2. 确定自己系统环境，编译对应版本的(nginx-flv),并且修改pom文件的ffmpeg对应的系统版本(图一)，以及yml文件的ip(图二)，图三为运行成功
![输入图片说明](image/image1.png)
![输入图片说明](image/image2.png)
![输入图片说明](image/image3.png)
3.项目自带测试工具，运行下图所示的代码
![输入图片说明](image/image4.png)
4.此时如果运行成功控制台会打印如下图所示，把打印的播放地址放到vlc即可播放，其他配置可以参考我写的注释，如果有不懂请加群
![输入图片说明](image/image8.png)
5.再没有修改代码之前支持flv，rtmp，后面两种hls，ws请修改一些参数即可

#### 简要说明
1.  针对刚接触jt1078流媒体的小伙伴，该项目拿来就可以用无需做过多的修改（目前只支持h264）
2.  支持转成rtmp，flv，hls，ws多种格式
3.  有兴趣的小伙伴可以加入群号：373203450
4. 测试服务器222.244.144.181:1078, 打开页面http://222.244.144.181:9995/@/，联系我开一个账号
5.  慷慨的宝子可以请我抽一根华子
这个是我开源的前端页面，可以借鉴参考
https://gitee.com/hui_hui_zhou/1078_video_vue

<img src="image/weixin.jpg" alt="输入图片说明" style="zoom: 25%;" /><img src="image/zfb.jpg" alt="输入图片说明" style="zoom: 25%;" />