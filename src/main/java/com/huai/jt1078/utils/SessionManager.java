package com.huai.jt1078.utils;

import io.netty.channel.ChannelHandlerContext;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.*;

/**
 * @author xingkong
 * @program jt1078
 * @description 单例Session管理器
 * @date 2021-09-06 17:21
 **/
public enum SessionManager {

    // 单例
    instance;

    /** 开始时间默认值 */
    public static final Long DEFAULT_TIMESTAMP = -1L;


    /**
     *  key: 013800138999-2
     *  value: ctx
     */
    @Getter
    private final Map<String, ChannelHandlerContext> tagChannelMap = Collections.synchronizedMap(new HashMap<>());

    /**
     * key: ctx
     * value: 013800138999-2
     */
    @Getter
    private final Map<ChannelHandlerContext, String> channelTagMap = Collections.synchronizedMap(new HashMap<>());

    /**
     * 视频工具类集合
     */
    @Getter
    private final Map<String,VideoUtil> videoUtils = Collections.synchronizedMap(new HashMap<>());

    /**
    * 数据推送目标地址
    */
    @Getter
    private final Map<String, ChannelHandlerContext> targetTagChannelMap = Collections.synchronizedMap(new HashMap<>());


    /**
     * RTMP推送流
     */
    @Getter
    private final Map<String, Process> processMap = Collections.synchronizedMap(new HashMap<>());

    /**
     * 音频通道标识
     */
    @Getter
    private final Set<String> AudioMap = Collections.synchronizedSet(new HashSet<>());

    @Getter
    private final Set<ChannelHandlerContext> contextSet = Collections.synchronizedSet(new HashSet<>());

    /**
     * websocket工具类
     */
    @Getter
    private final Map<String, WebsocketUtil> websocketUtilMap = Collections.synchronizedMap(new HashMap<>());

    @Getter
    @Setter
    private RedisTemplate<String, String> redisTemplate = null;


}
