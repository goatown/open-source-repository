package com.huai.jt1078.entity;

/**
 *
 * @author houcheng
 * @date 2019-12-11
 * 数据流，可能是视频或是音频，视频为FLV封装，音频为PCM编码的片断
 */
public class Media {
    public enum Type { Video, Audio };

    public MediaEncoding.Encoding encoding;
    public long sequence;
    public byte[] data;

    public Media(long seq, MediaEncoding.Encoding encoding, byte[] data)
    {
        this.data = data;
        this.encoding = encoding;
        this.sequence = seq;
    }
}
