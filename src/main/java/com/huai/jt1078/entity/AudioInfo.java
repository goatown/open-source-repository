package com.huai.jt1078.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author xingkong
 * @program jt1078
 * @description 音频信息
 * @date 2021-09-16 11:33
 **/
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class AudioInfo {


    /** 负载类型 */
    private Integer loadType;

    /** 时间戳 */
    private Integer timeStamp;

    /** 序号 */
    private Integer sequence;

    /** sim卡号 */
    private String simNo;

    /** 通道号 */
    private Integer channelNo;

    /** 由pcm转换好的音频数据 */
    private byte[] data;
}
