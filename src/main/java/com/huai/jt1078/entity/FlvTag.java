package com.huai.jt1078.entity;

//由于flv格式是header+body,而body是preTagSize+currentTagData.......循环形式,并且第一个的preTagSize始终为0,
// 与其这样不如采用(header+preTag0Size)+（currentTagData+currentTagSize），这样就避免每次都要记录以下上一个tag的数据大小。

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * author:zhouyili (11861744@qq.com)
 * @author Administrator
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FlvTag {
    public static final int AUDIO = 8;
    public static final int VIDEO = 9;
    //0x12
    public static final int SCRIPT = 18;
    private int preTagSize;
    private byte tagType;
    /**3个字节 streamId以后的数据长度*/
    private int tagDataSize;
    /** 低3个字节写入后再写高位字节,相对于第一帧的时间偏移量，单位ms */
    private int offSetTimestamp;
    /** 3个字节，一般总是0 */
    private int streamId;

    public FlvTag(int offSetTimestamp, int tagDataSize) {
        this.tagDataSize = tagDataSize;
        this.offSetTimestamp = offSetTimestamp;
    }

}
