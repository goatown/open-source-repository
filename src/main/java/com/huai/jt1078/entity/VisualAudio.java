package com.huai.jt1078.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 音视频公共类
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class VisualAudio {

    /**
     * 负载类型
     */
    private Integer loadType;

    /**
     * 包序号
     */
    private Integer sequence;

    /**
     * SIM卡号
     */
    private String simNo;

    /**
     * 通道号
     */
    private Integer channelNo;

    /**
     * 数据类型
     */
    private Integer dataType;

    /**
     * 分包标记
     */
    private Integer subTag;

    /**
     * 时间戳
     */
    private Long timeStamp;

    /**
     * 距离上一个关键帧 单位毫秒
     */
    private Integer lastKeyFrame;

    /**
     * 距离上一个帧 单位毫秒
     */
    private Integer lastFrame;

    /**
     * 数据长度
     */
    private Integer dataLength;

    /**
     * 数据体
     */
    private byte[] data;

}
