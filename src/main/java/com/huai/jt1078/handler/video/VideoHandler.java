package com.huai.jt1078.handler.video;

import com.huai.jt1078.entity.VisualAudio;
import com.huai.jt1078.enums.VisualAudioType;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author xingkong
 * @program jt1078
 * @description 视频处理类
 * @date 2021-09-06 10:56
 **/
@Slf4j
@Service
public class VideoHandler extends VisualAudioHandler{

    private final Map<String, String> naluMap = Collections.synchronizedMap(new HashMap<String, String>());

    @Override
    protected VisualAudioType getClassName() {
        return VisualAudioType.VIDEO;
    }

    @Override
    public void handler(VisualAudio visualAudio, ChannelHandlerContext ctx){
        super.init(visualAudio,ctx);
        // 推送视频流
        videoUtil.publishVideo(visualAudio);


    }
}
