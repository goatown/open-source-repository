package com.huai.jt1078.handler.video;

import cn.hutool.core.util.ObjectUtil;
import com.huai.jt1078.config.VideoConfig;
import com.huai.jt1078.entity.VisualAudio;
import com.huai.jt1078.enums.VisualAudioType;
import com.huai.jt1078.utils.SessionManager;
import com.huai.jt1078.utils.VideoUtil;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;

/**
 * @author xingkong
 * @program jt1078
 * @description 音视频处理器
 * @create 2021-09-06 10:51
 **/
@Slf4j
public abstract class VisualAudioHandler {

    public VideoUtil videoUtil;
    @Resource
    VideoConfig videoConfig;

    public void init(VisualAudio visualAudio,ChannelHandlerContext ctx){
        String tag = visualAudio.getSimNo() +"-"+ visualAudio.getChannelNo();
        videoUtil = SessionManager.instance.getVideoUtils().get(tag);
        // 如果当前不存在对应的视频处理工具类 则创建一个视频工具类
        if (ObjectUtil.isNull(videoUtil)){
            // 指定通道和tag的映射关系
            SessionManager.instance.getChannelTagMap().put(ctx,tag);
            SessionManager.instance.getTagChannelMap().put(tag,ctx);
            videoUtil = new VideoUtil(tag,videoConfig);
            SessionManager.instance.getVideoUtils().put(tag,videoUtil);
        }else{
            // 校验是否已存在对应的ctx
            ChannelHandlerContext context = SessionManager.instance.getTagChannelMap().get(tag);
            if (!context.equals(ctx)){
                throw new RuntimeException("当前通道已被占用");
            }
        }
    }

    /**
     * 返回类名
     * @author xingkong
     * @date 2021-09-06 10:54
     * @return 类名
     */
    protected abstract VisualAudioType getClassName();

    /**
     * 业务处理
     * @param visualAudio 音视频数据
     * @author xingkong
     * @date 2021-09-06 11:15
     */
    public abstract void handler(VisualAudio visualAudio,ChannelHandlerContext ctx);
}
