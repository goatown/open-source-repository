package com.huai.jt1078.handler.video;

import com.huai.jt1078.entity.VisualAudio;
import com.huai.jt1078.enums.VisualAudioType;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author xingkong
 * @program jt1078
 * @description 对讲处理器
 * @date 2021-09-16 14:09
 **/
@Slf4j
@Service
public class IntercomHandler extends VisualAudioHandler{

    @Override
    protected VisualAudioType getClassName() {
        return VisualAudioType.INTERCOM;
    }

    @Override
    public void handler(VisualAudio visualAudio, ChannelHandlerContext ctx) {
        super.init(visualAudio,ctx);
        log.info("接收到透传：SIM卡号：{}  通道号：{}",visualAudio.getSimNo(),visualAudio.getChannelNo());
    }
}
