package com.huai.jt1078.enums;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xingkong
 * @program jt1078
 * @description: NALU的类型
 * @date 2021-09-06 21:31
 **/
public enum NaluType {
    // NAL头 0位是F 初始为0 1~2位保存这NRI 标识重要等级（暂无实用） 3~7位保存NAL类型
    // 0 未指定
    // 1 非IDR的片
    // 2 片数据A分区
    // 3 片数据B分区
    // 4 片数据C分区
    // 5 IDR图像的片
    // 6 补充增强信息单元（SEI）
    // 7 序列参数集（SPS）
    // 8 图像参数集（PPS）
    // 9 分界符
    // 10 序列结束
    // 11 码流结束
    AB_IDR(0x01,"非IDR的片"),
    DATA_A(0x02,"片数据A分区"),
    DATA_B(0x03,"片数据B分区"),
    DATA_C(0x04,"片数据C分区"),
    IDR(0x05,"IDR图像的片"),
    SPS(0x07,"序列参数集（SPS）"),
    PPS(0x08,"图像参数集（PPS）");


    @Getter
    private final Integer code;

    @Getter
    private final String label;

    NaluType(Integer code, String label) {
        this.code = code;
        this.label = label;
    }

    public static List<Integer> naluTypes(){
        List<Integer> types = new ArrayList<>();
        for (NaluType type : values()){
            types.add(type.code);
        }
        return types;
    }
}
