package com.huai.jt1078;

import java.util.Arrays;

/**
 * @author xingkong
 * @program jt1078
 * @description
 * @date 2021-09-22 18:16
 **/
public class Test4 {

    public static void main(String[] args) {
        int[] a = new int[]{1,2,3,4,5,6,7};
        int[] b = new int[a.length - 2];
        System.arraycopy(a,2,b,0,b.length);
        System.out.println(Arrays.toString(b));
    }
}
