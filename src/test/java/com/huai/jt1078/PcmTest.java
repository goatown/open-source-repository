package com.huai.jt1078;

import com.huai.jt1078.codec.AudioCodec;
import com.huai.jt1078.utils.SessionManager;
import com.huai.jt1078.utils.VideoUtil;
import io.netty.channel.ChannelHandlerContext;

import java.io.InputStream;

/**
 * @author xingkong
 * @program jt1078
 * @description 推送视频流
 * @date 2021-09-15 16:05
 **/
public class PcmTest {

    public static void main(String[] args) throws Exception{
        String tag = "018106772356-2";
        InputStream fis = VideoPushTest.class.getResourceAsStream("e://output.pcm");
        ChannelHandlerContext context = SessionManager.instance.getTagChannelMap().get(tag);
        VideoUtil videoUtil = SessionManager.instance.getVideoUtils().get(tag);
        AudioCodec audioCodec = videoUtil.getAudioCodec();
        int len = -1;
        byte[] block = new byte[512];
        while ((len = fis.read(block)) > -1)
        {
            context.writeAndFlush(audioCodec.fromPCM(block));
            Thread.sleep(20);
            System.out.println("sending...");
        }
    }
}
